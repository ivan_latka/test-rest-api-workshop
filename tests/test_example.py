# this is ad example of get api test, which verif
import requests


def test_detail_of_customer(base_url):
    customer_response = requests.get(url=base_url + 'customers', params={'id': 1})
    assert customer_response.status_code == 200

    customer_json = customer_response.json()
    assert isinstance(customer_json, list)
    assert len(customer_json) == 1
    assert customer_json[0] == {
          "id": 1,
          "first_name": "Testerus",
          "last_name": "Regressus",
          "phone": "123 456 789"
    }
