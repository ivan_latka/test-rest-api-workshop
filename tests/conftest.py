# Example of pytest fixture
import pytest
from  tests import settings


@pytest.fixture
def base_url():
    return 'http://{}:3000/'.format(settings.api_host)
