# Tests of REST API Workshop #


## Author
Ivan Latka, evee.latkinson@gmail.com


# Goal
Goal of this workshop is to give you an example of how to test API from
python. Project is using several tools, which be a bit overwhelming for
newcomers. Please, do not hesitate and ask any question if you feel
stuck. :-)


## Setup project
1. Install [python 3.5 or 3.6](https://www.python.org/downloads/)
(or at least any python 3).
2. Download this workshop project from bitbucket.org.
    - Download workshop [directly](https://bitbucket.org/ivan_latka/test-rest-api-workshop/downloads/)
    and just unzip it somewhere into your PC. Rename final directory to
    `test-rest-api-workshop`.
    - (Optional) If you have [git](https://git-scm.com/downloads) installed you can
    use command:
    ```
    git clone https://ivan_latka@bitbucket.org/ivan_latka/test-rest-api-workshop.git
    ```
3. Python IDE:
    - Install [Pycharm Community Edition](https://www.jetbrains.com/pycharm/download).
    - Open downloaded workshop as a new project in Pycharm.
4. (Optional) Setup python [virtual environment](https://virtualenv.pypa.io/en/stable/installation/)
    and run following step inside virtual environment.
5. Inside workshop dir, run command:
    ```
    pip install -r requirements.txt
    ```

## Call API
1. First try to call http://172.18.10.94:3000/customers from your browser.
    You should get a list of customers.
2. Now open python console a try to call the same request from python.
    ```
    import requests;
    from tests import settings;
    response = requests.get('http://{}:3000/customers'.format(settings.api_host));
    assert response.status_code == 200;
    ```
3. Congratulations! You just used [requests library](http://docs.python-requests.org/en/master/).
    Look on its [basic examples](http://docs.python-requests.org/en/master/user/quickstart/#make-a-request).
4. Also look at [basic examples](https://coligo.io/create-mock-rest-api-with-json-server/)
    of our workshop [json server](https://github.com/typicode/json-server).
    Try to call POST and PUT methods and create and edit new customer.


## Run Example Test
1. Look into file `tests/tests_example.py`. You should see example of the first
    test in there. Here you can find [basic examples of the pytest library](https://docs.pytest.org/en/latest/getting-started.html#our-first-test-run).
2. Now look into file `tests/conftest.py`. It contains pytest fixture,
    which is used in previously mentioned test. Here are more details
    about [pytest fixtutes](https://docs.pytest.org/en/latest/fixture.html#fixtures).
3. Go to `Pycharm > File > settings > Tools > Python inttegrated tools`
    and set default test runner to `py.test`.
4. Open file `test_example.py` in pycharm and click
    `Run > Run 'py.test' for test_example ...`.
    In ideal circumstances, test should pass. :-)

## Write Your Own Tests
1. Test POST of a new customer and then GET and assert detail of this new customer.
2. Test PUT (update) of existing customer.
3. Write pytest fixture, which will create new customer.
    Use this fixture in previous two tests.
4. Write teardown fixture which will delete customer from previous fixture.
5. Refactor `base_url` fixture into a session scope fixture.
6. Create python class with methods for getting, posting, putting
    and deleting of customer. Use these methods in previous tests.
7. Read about [parametrized pytest tests](https://docs.pytest.org/en/latest/parametrize.html)
    and write test of


## Links
- [Readme Markdown](https://bitbucket.org/tutorials/markdowndemo)